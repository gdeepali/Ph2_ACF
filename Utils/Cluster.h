/*
 
 \file                          Cluster.h
 \brief                         Cluster handling from DAQ
 \author                        Florian AUBIN
 \version                       1.0
 \date                                  15/06/16
 \Support :                      mail to : florian.aubin@cern.ch
 
*/


#include <string>
#include <bitset>
#include <sstream>
#include <cstring>
#include <iomanip>
#include <numeric>
#include "ConsoleColor.h"
#include "../HWDescription/Definition.h"
#include "../HWDescription/BeBoard.h"

using namespace Ph2_HwDescription;

namespace Ph2_HwInterface
{

 	/*!
         * \class Cluster
         * \brief Cluster of Hits
         */
        class Cluster
        {
		private:
			uint32_t position;
			uint32_t width;
		
		public:

		private:
			/*!
          		 * \brief Set a position of the Cluster Class
  			 * \param pposition
 	 	         */
			void SetPosition (uint32_t pposition);

			/*!
          		 * \brief Set a width of the Cluster Class
 			 * \param pwidth
  	 	         */
			void SetWidth (uint32_t pwidth);
			
	        public:
		
		        /*!
      			 * \brief Constructor of the Cluster Class
  		    	 * \param pposition
  			 * \param pwidth
          		 */
			Cluster(uint32_t pposition, uint32_t pwidth);
       			
			/*!
          		 * \brief Copy Constructor of the Cluster Class
 		         */			
			Cluster(const Cluster& pCluster);
			
			/*!
          		 * \brief Destructor of the Cluster Class
  		         */		
			~Cluster(){
			}	
			
			/*
			 * \brief Function to get position of the Cluster
			 */
			uint32_t GetPosition () const;
			
			/*
			 * \brief Function to get width of the Cluster
			 */
			uint32_t GetWidth () const;		
			
	};
}
