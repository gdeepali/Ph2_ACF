/*
	\file				Cluster.cc
        \brief        	                Cluster handling from DAQ
        \author                         Florian AUBIN
        \version                        1.0
        \date                           15/06/16
        Support :                       mail to : florian.aubin@cern.ch

 */

#include "../Utils/Cluster.h"

using namespace Ph2_HwDescription;

namespace Ph2_HwInterface
{

	Cluster(uint32_t pposition, uint32_t pwidth)
	{
		SetPosition(pposition);
		SetLength(pwidth);
	}

	Cluster(const Cluster& pCluster)
	{
		SetPosition(pClusteur.position);
		SetWidth(pClusteur.width);
	}

	void SetFirtEvent (uint32_t pposition)
	{
		Cluster.position = pposition;
	}

	void SetLength (uint32_t ppwidth)
	{
		Cluster.width = ppwidth;
	}

	uint32_t GetPosition () const
	{
		return Cluster.position;
	}
	
	uint32_t GetWidth () const
	{
		return Cluster.width;
	}	

}
